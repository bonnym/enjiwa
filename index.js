// import { AppRegistry } from 'react-native';
// import App from './App';
//
// AppRegistry.registerComponent('njiwa', () => App);

'use strict'
import React, { Component } from 'react';
import { AppRegistry, View, Text, StyleSheet, Dimensions } from 'react-native';
import Root from "./src/main";

import codePush from 'react-native-code-push';

class njiwa extends Component {

// componentDidMount() {
//     codePush.sync({
//         updateDialog: true,
//         installMode: codePush.InstallMode.IMMEDIATE
//     });
// }

    render() {
        return (
            <View style={styles.container}>
                <Root {...this.props} />
            </View>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1
    }
});


AppRegistry.registerComponent('njiwa', () => njiwa);

