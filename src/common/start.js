import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    ImageBackground,
    Image,
    Dimensions,
    TouchableOpacity,
    View
} from 'react-native';

let { height, width } = Dimensions.get('window');

export default class Start extends Component {
    render() {
        return (
            <ImageBackground style={styles.container} resizeMode="stretch" source={require('../assets/images/party.jpg')}>
                <View style={{flex:8, alignItems:'flex-end', justifyContent:'center'}}>
                    <View style={{width:((width/2) + 40), padding:5, borderTopLeftRadius:40, borderBottomLeftRadius:40, flexDirection:'row', backgroundColor:'rgba(0,0,0,0.5)', alignItems:'center'}}>
                        <TouchableOpacity style={styles.circle} onPress={() =>this.props.navigation.navigate('tabs')}>
                            <Image source={require('../assets/images/bird.png')} resizeMode="contain" style={{height:30, width:30}} />
                        </TouchableOpacity>
                        <Text style={{color:'#fff', fontSize:19, margin:12}}>Get Started</Text>
                    </View>
                    <Image source={require('../assets/logo.png')} resizeMode="contain" style={{height:50, width:200, margin:20, alignSelf:'center'}} />
                </View>
                <TouchableOpacity style={{flex:1, backgroundColor:'rgba(0,0,0,0)', alignItems:'center', padding:10}} onPress={() =>this.props.navigation.navigate('login')}>
                    <Text style={{color:'#e7e7e7', fontSize:17}}>
                        Already Have An Account?
                    </Text>
                    <Text style={{color:'#8dd7ff', fontSize:18}}>
                        Sign In
                    </Text>
                </TouchableOpacity>
            </ImageBackground>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        height:null,
        width:null,

        backgroundColor: '#F5FCFF',
    },
    circle:{
        backgroundColor:'rgba(85,172,239,0.2)',
        height:60,
        width:60,
        alignItems:'center',
        justifyContent:'center',
        borderRadius:30,

    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});

