import React from "react";
import { Text } from "react-native";
import { Footer, FooterTab, Button} from "native-base";
import {Icon, Row} from '@shoutem/ui';
import styles from "./index";


export const FooterComponent =  ({logo})=>{

    //tab bar items
    const tabs = [{
        title:"TaxiCar",
        subTitle:"",
        Icon:"settings"
    },
        {
            title:"express",
            subTitle:"",
            Icon:"car"
        },
        {
            title:"Premium",
            subTitle:"",
            Icon:"car"
        },{
            title:"Bike",
            subTitle:"",
            Icon:"car"
        }];

    return (
        <Footer>
            <FooterTab style={styles.footerContainer} >

                {
                    tabs.map((obj, index)=>{
                        return (
                            <Button key={index}>
                                <Icon size={20} name={obj.icon} color={(index === 0) ? "#FF5E3A" : "grey"} />
                                <Text style={{fontSize:12, color:(index === 0) ? "#FF5E3A" : "grey"}}>{obj.title}</Text>
                                <Text style={styles.subText}>{obj.subTitle}</Text>
                            </Button>

                        )
                    })
                }

            </FooterTab>
        </Footer>
    );
};

export default FooterComponent;