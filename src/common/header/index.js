import React from "react";
import { Text, Image, StatusBar } from "react-native";
import { Header, Left, Body, Right, Button} from "native-base";
import styles from "./styles";
import { Icon } from 'react-native-elements'
// import Icon from "react-native-vector-icons/FontAwesome";





export const HeaderComponent =  ({logo})=>{

    function go2Notifications(){
        this.props.navigation.navigate('notifications')
    }

    return (

        <Header style={{backgroundColor:"orange"}} >
            <Left>
                <Button transparent>
                    <Icon name="list" size={30} style={styles.icon}/>
                </Button>
            </Left>
            <Body>{logo &&
            <Image resizeMode="contain" style={styles.logo} source={logo}/>
            ||
            <Text style={styles.headerText}>Driver on the way</Text>
            }
            </Body>

            <Right>
                <Button transparent onPress={this.go2Notifications}>
                    <Icon name="notifications" size={30} style={styles.icon}  />
                </Button>
            </Right>
        </Header>
    );
};

export default HeaderComponent;