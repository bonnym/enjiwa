import { combineReducers } from 'redux'
// import { HomeReducer as tracker } from "../routes/tracker/modules/home"
import { LoginReducer as login } from "../routes/login/modules/login_modules"


export const makeRootReducer = () => {
    return combineReducers({
        // tracker,
        login
    });
};

export default makeRootReducer ;