import { createStore, applyMiddleware, compose } from 'redux'
// import rootReducer from './reducers'
import makeRootReducer from './reducers'
import thunk from "redux-thunk"
import { createLogger } from "redux-logger"
import { composeWithDevTools } from 'remote-redux-devtools'
import promiseMiddleware from 'redux-promise-middleware'

const log =  createLogger({ diff: true, collapsed: true });



export default (initialState = {}) => {

    // ====================================
    // Middleware configuration
    // ====================================
    const middleware = [thunk, log];

    // ====================================
    // Store Enhancers
    // ====================================
    const enhancers = [];

    // ====================================
    // Store Instatiation
    // ====================================
    const store = createStore(
        makeRootReducer(),
        initialState,
        compose(
            applyMiddleware(...middleware),
            ...enhancers
        )
    );

    return store;

}
















// const store  = createStore(rootReducer, middleware);

// export default store;

// const enhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__  || compose;
//
// const middleware =  enhancers(applyMiddleware(promiseMiddleware(), thunk, log ));
//
// export default createStore(
//         rootReducer,
//         undefined,
//         middleware
// );