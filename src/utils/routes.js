import { Icon } from 'react-native-elements';
import { StackNavigator, TabNavigator } from 'react-navigation';

import Login from '../routes/login/components/index';
import Start from '../common/start';
import Map from "../routes/map/components/map";
import Home from "../routes/home/index";
import Me from "../routes/me/index";
import Settings from "../routes/settings/index";
import Stamps from "../routes/stamps/components/stamps";
import Search from "../routes/search/index";
import Philately from "../routes/philately/index";
import Tracker from "../routes/track/index";
import Notifications from "../routes/notifications/index";
import Scan from "../routes/scan/index";
import More from "../routes/more/index";


export const Tabs = TabNavigator({
    home: {
        screen: Home,
        navigationOptions: {
            label: 'Home',
            header: null,
            icon: ({tintColor}) => <Icon name="list" size={35} color={tintColor} />
        }
    },

    Stamps: {
        screen: Stamps,
        navigationOptions: {
            label: 'Stamps',
            header: null,
            icon: ({tintColor}) => <Icon name="list" size={35} color={tintColor} />
        }
    },

    Search: {
        screen: Search,
        navigationOptions: {
            label: 'Search',
            header: null,
            icon: ({tintColor}) => <Icon name="search" size={35} color={tintColor} />
        }
    },


    Me: {
        screen: Me,
        navigationOptions: {
            label: 'Me',
            header: null,
            icon: ({tintColor}) => <Icon name="account-circle" size={35} color={tintColor} />
        }
    }
});


export const SettingStack = StackNavigator({
    Settings: {
        screen: Settings,
        navigationOptions: {
            title: 'Settings',
        }
    }
});



export const Root = StackNavigator({

    Tabs: {
        screen: Tabs
    },

    settings: {
        screen: SettingStack
    },

}, {
    mode: 'modal',
    headerMode: 'none',
});




export const Navigator = StackNavigator({

    start: {
        screen: Start,
        navigationOptions:{
            header: null
        }
    },

    root: {
        screen: Root,
        navigationOptions: {
            header: null
        }
    },

    tabs: {
        screen: Tabs,
        navigationOptions: {
            // icon: require('../assets/logo.png')
            title: 'e-Njiwa Solutions'
        }
    },


    notifications: {
        screen: Notifications,
        navigationOptions: {
            title: 'Notifications'
        }
    },

    scan: {
        screen: Scan,
        navigationOptions: {
            title: 'Locate parcel by scanning'
        }
    },


    philately: {
        screen: Philately,
        navigationOptions:{
            title: 'Philatelic Stamps'
        }
    },

    login: {
        screen: Login,
        navigationOptions: {
            header: null
        }
    },

    more: {
        screen: More,
        navigationOptions: {
            title: 'More'
        }
    },

    tracker: {
        screen: Tracker,
        navigationOptions: {
            title: 'Tracker'
        }
    },

    map: {
        screen: Map,
        navigationOptions: {
            header: null
        }
    }

});

export default Navigator