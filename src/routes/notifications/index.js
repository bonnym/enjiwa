import React, { Component } from 'react'
import {
    ImageBackground,
    View,
    ScrollView
} from 'react-native'

import {
    Fab,
    Container,
    Footer,
    FooterTab
} from 'native-base'

import {
    Screen,
    DropDownMenu,
    Text,
    NavigationBar,
    Image,
    Title,
    Icon,
    Row,
    Subtitle,
    Caption,
    Divider,
    TouchableOpacity
} from '@shoutem/ui'

import {
    Button,
    SearchBar,
    Avatar
} from 'react-native-elements'

import styles from './styles'

const logo = require('../../assets/logo.png');


export default class Notifications extends Component{

    constructor(props){
        super(props);
        this.state = {
            cars: [
                { title: 'RECEIVED', value: 'RECEIVED' },
                { title: 'SENT', value: 'SENT' },
                { title: 'UNREAD', value: 'UNREAD' },
            ],
            active: false
        }
    }


    render(){
        return(
            <Container>
                <SearchBar
                />
                <ScrollView>

                    <TouchableOpacity
                        style={{alignItems: 'center', flexDirection: 'row', backgroundColor: 'white'}}
                        onPress={() =>this.props.navigation.navigate('Stamp')}
                    >
                        <Avatar
                            rounded
                        />
                        <Image
                            style={{marginRight: 9}}
                            styleName="small rounded-corners"
                            source={{ uri: 'https://shoutem.github.io/img/ui-toolkit/examples/image-2.png' }}
                        />
                        <View styleName="vertical stretch space-between">
                            <Subtitle> Malaika Firth, Mombasa </Subtitle>
                            <Caption> 59000-00100 | asldfk </Caption>
                        </View>
                    </TouchableOpacity>

                    <Divider styleName={'line'} />

                    <TouchableOpacity
                        style={{alignItems: 'center', flexDirection: 'row', backgroundColor: 'white'}}
                        onPress={() =>this.props.navigation.navigate('Stamp')}
                    >
                        <Avatar
                            rounded
                        />
                        <Image
                            style={{marginRight: 9}}
                            styleName="small rounded-corners"
                            source={{ uri: 'https://shoutem.github.io/img/ui-toolkit/examples/image-2.png' }}
                        />
                        <View styleName="vertical stretch space-between">
                            <Subtitle> Racheal Zane, Nakuru </Subtitle>
                            <Caption> 58080-00200 | asldfk </Caption>
                        </View>
                    </TouchableOpacity>

                </ScrollView>

                <View style={styles.fareContainer}>
                    <Text>
                        <Text style={styles.fareText}> STATUS: </Text> <Text style={styles.amount}> You have read all your notifications</Text>
                    </Text>

                </View>

            </Container>
        )
    }

}













