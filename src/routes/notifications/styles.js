import { Dimensions } from "react-native";
const { width } = Dimensions.get("window");

const styles = {
    footerContainer:{
        backgroundColor:"#fff",
        alignItems: 'center',
        marginLeft: 3
    },
    subText:{
        fontSize:8
    },

    fareContainer: {
        width:width,
        height:30,
        padding:10,
        backgroundColor:"grey",
        justifyContent: 'center'
    },
    fareText: {
        fontSize: 12
    },
    amount:{
        fontWeight:"bold",
        fontSize: 12
    }

};

export default styles;