import React, { Component } from 'react'
import { connect } from 'react-redux'
import {
    View,
    ScrollView,
    AsyncStorage,
    ImageBackground
} from 'react-native'

import {
    Screen,
    DropDownMenu,
    Text,
    NavigationBar,
    Image,
    Title,
    Row,
    Subtitle,
    Caption,
    Divider,
    Button,
    Card,
    Tile,
    Overlay,
    Heading
} from '@shoutem/ui'

import { Container, Fab } from 'native-base';
// import Icon from 'react-native-vector-icons/dist/FontAwesome'
import { Icon, SearchBar } from 'react-native-elements'

export default class Philately extends Component{

    constructor(props) {
        super(props)
        this.state = {
            active: false
        };
    }

    logOut() {
        AsyncStorage.removeItem('user');
        this.props.navigation.navigate('Login');
    }

    render(){
        return(
            <Screen>

                <SearchBar/>

                <ScrollView style={{marginLeft: 5}} >

                    <Tile>
                        <Title styleName="md-gutter-top">COOL BLACK AND WHITE STYLISH WATCHES</Title>
                        <Heading>KES 250.00</Heading>
                        <Button styleName="md-gutter-top"><Icon name="cart" /><Text>ADD TO BASKET</Text></Button>
                    </Tile>

                    <Divider styleName="section-header" />


                    <Tile>
                        <Title styleName="md-gutter-top">COOL BLACK AND WHITE STYLISH WATCHES</Title>
                        <Heading>KES 500.00</Heading>
                        <Button styleName="md-gutter-top"><Icon name="cart" /><Text>ADD TO BASKET</Text></Button>
                    </Tile>


                </ScrollView>

                <Fab
                    active={this.state.active}
                    direction="up"
                    containerStyle={{ }}
                    style={{ backgroundColor: '#5067FF' }}
                    position="bottomRight"
                    onPress={() => this.setState({ active: !this.state.active })}>
                    <Icon name="telegram" />
                    <Button style={{ backgroundColor: 'red' }}>
                        <Icon name="plus" onPress={() => this.logOut()}/>
                    </Button>
                    <Button style={{ backgroundColor: 'dodgerblue' }}>
                        <Icon name="share" />
                    </Button>
                    <Button style={{ backgroundColor: 'orange' }}>
                        <Icon name="plus" />
                    </Button>
                </Fab>

            </Screen>
        )
    }

}