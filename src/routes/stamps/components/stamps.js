import React, { Component } from 'react'
import {
    ImageBackground,
    View,
    ScrollView
} from 'react-native'

import {
    Screen,
    DropDownMenu,
    Text,
    NavigationBar,
    Image,
    Title,
    Icon,
    Row,
    Subtitle,
    Caption,
    Divider,
    TouchableOpacity
} from '@shoutem/ui'

import {
    Tile,
    List,
    ListItem,
    Button
} from 'react-native-elements'
import HeaderComponent from "../../../common/header/index";

const logo = require('../../../assets/logo.png');


export default class Stamps extends Component{

    constructor(props){
        super(props);
        this.state = {
            cars: [
                { title: 'RECEIVED', value: 'RECEIVED' },
                { title: 'SENT', value: 'SENT' },
                { title: 'UNREAD', value: 'UNREAD' },
            ],
        }
    }


    render(){
        return(
            <Screen style={{marginTop: 20}}>
                <DropDownMenu
                    styleName="horizontal"
                    options={this.state.cars}
                    selectedOption={this.state.selectedCar ? this.state.selectedCar : this.state.cars[0]}
                    titleProperty="title"
                    onOptionSelected={(car) => this.setState({ selectedCar: car })}
                    valueProperty="value"
                />

                <Text>{this.state.selectedCar ? this.state.selectedCar.value : this.state.cars[0].value}</Text>

                <ScrollView>

                    <TouchableOpacity
                        style={{alignItems: 'center', flexDirection: 'row', backgroundColor: 'white'}}
                        onPress={() =>this.props.navigation.navigate('Stamp')}
                    >
                        <Icon name="notifications" style={{color: 'skyblue'}} />
                        <Image
                            style={{marginRight: 9}}
                            styleName="small rounded-corners"
                            source={{ uri: 'https://shoutem.github.io/img/ui-toolkit/examples/image-2.png' }}
                        />
                        <View styleName="vertical stretch space-between">
                            <Subtitle>From: Malaika Firth, Mombasa</Subtitle>
                            <Caption> 6 hours ago | 8:30 AM </Caption>
                        </View>
                    </TouchableOpacity>

                    <Divider styleName={'line'} />

                    <TouchableOpacity
                        style={{alignItems: 'center', flexDirection: 'row', backgroundColor: 'white'}}
                        onPress={() =>this.props.navigation.navigate('Stamp')}
                    >
                        <Icon name="notifications" style={{opacity: .5, color: 'black'}} />
                        <Image
                            style={{marginRight: 9}}
                            styleName="small rounded-corners"
                            source={{ uri: 'https://shoutem.github.io/img/ui-toolkit/examples/image-2.png' }}
                        />
                        <View styleName="vertical stretch space-between">
                            <Subtitle>From: Racheal Zane, Nakuru</Subtitle>
                            <Caption> 6 hours ago | 8:30 AM </Caption>
                        </View>
                    </TouchableOpacity>

                </ScrollView>
            </Screen>
        )
    }

}













