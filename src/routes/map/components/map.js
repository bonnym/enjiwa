import React, { Component } from "react";
import {View, Text, StyleSheet} from "react-native";
import { Container }  from "native-base";
import MapContainer from "./map_container/index";
import HeaderComponent from "../../../common/header/index";
import FooterComponent from "../../../common/footer/index";
import Fab from "./fab/index";

const logo = require('../../../assets/logo.png');

export default class Map extends Component{

    render(){

        return(

            <Container>
                <View style={{flex:1}}>
                    <HeaderComponent logo={logo} />
                    <MapContainer
                    />
                    <Fab/>
                    <FooterComponent logo={logo}/>
                </View>
            </Container>
        )
    }
}
