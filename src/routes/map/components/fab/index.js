import React from "react";
import {Text} from "react-native";
import { View, Button } from "native-base";

import styles from "./styles";

export const Fab = ({})=>{
    return (
        <Button style={styles.fabContainer}>
            <Text style={styles.btnText}> Request </Text>
        </Button>

    );
};

export default  Fab;