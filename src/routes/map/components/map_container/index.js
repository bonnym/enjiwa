import React, { Component } from "react";
import {View, Text, StyleSheet} from "react-native";
import MapView from 'react-native-maps';
import { Container }  from "native-base";
import styles from './styles'
import SearchBox from "../search_box/index";


export default class MapContainer extends Component{

    render(){

        return(

            <View style={styles.container}>
                <MapView
                    provider={MapView.PROVIDER_GOOGLE}
                    style={styles.map}
                />
                <SearchBox/>
            </View>
        )
    }
}
