import React, { Component } from 'react'
import {
    Image,
    ImageBackground,
    View,
    Text,
    ScrollView,
    AsyncStorage,
    Switch
} from 'react-native'
import {
    Row,
    TouchableOpacity,
    Card
} from '@shoutem/ui';

import {
    Tile,
    ListItem,
    List,
    Button,
    Icon,
} from 'react-native-elements'




export default class Settings extends Component{

    constructor(props){
        super(props);
        this.state = {

        }
    }

    logOut() {
        AsyncStorage.removeItem('user');
        this.props.navigation.navigate('start');
    }

    render(){
        return(
            <ScrollView>

                <List style={{flexDirection: 'horizontal'}}>
                    <Text> Notifications </Text>
                    <Switch/>
                </List>
                <List style={{flexDirection: 'horizontal'}}>
                    <Text> Notification tone </Text>
                    <Switch/>
                </List>
                <List style={{flexDirection: 'horizontal'}}>
                    <Text> Vibrate </Text>
                    <Switch/>
                </List>

                <List style={{flexDirection: 'horizontal'}}>
                    <Text> Delete my account </Text>
                    <Switch />
                    {/*alert('this will allow anyone to see your address')*/}

                </List>

                <TouchableOpacity
                    onPress={() => this.logOut()}
                    style={{height: 50, backgroundColor: 'dodgerblue', marginTop: 5}}
                >
                    <Text style={{color: 'white', fontSize: 20, fontWeight: '500', fontStyle: 'italic', textAlign: 'center', padding: 10}}> Lougout </Text>
                </TouchableOpacity>

            </ScrollView>
        )
    }

}
