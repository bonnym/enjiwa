import React, { Component } from 'react'
import {
    ImageBackground,
    View,
    Text,
    ScrollView
} from 'react-native'
import {
    Icon,
    Row,
    Card,
    TouchableOpacity,
    Subtitle,
    Image,
    Caption,
} from '@shoutem/ui';

import {
    Tile,
    List,
    ListItem,
    // Button,
    Avatar
} from 'react-native-elements'




export default class Me extends Component{

    constructor(props){
        super(props);
        this.state = {

        }
    }


    render(){
        return(
            <ScrollView>
                <Tile
                    imageSrc={require('../../assets/images/lady.png')}
                    featured
                    title="Zane Obwoni"
                    caption="5900-00100@postakenya.post"
                />
                <TouchableOpacity
                    onPress={() => {this.props.navigation.navigate('settings')}}
                    style={{height: 50, backgroundColor: 'dodgerblue', marginTop: 5}}
                >
                    <Text style={{color: 'white', fontSize: 20, fontWeight: '500', fontStyle: 'italic', textAlign: 'center', padding: 10}}> Settings </Text>
                </TouchableOpacity>

                <View>
                    <TouchableOpacity
                        style={{alignItems: 'center', flexDirection: 'row', backgroundColor: 'white', height: 60, marginTop: 6}}
                    >
                        <View styleName="vertical stretch space-between">
                            <Subtitle> Username </Subtitle>
                            <Caption> 58080-00200 </Caption>
                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity
                        style={{alignItems: 'center', flexDirection: 'row', backgroundColor: 'white', height: 60, marginTop: 6}}
                    >
                        <View styleName="vertical stretch space-between">
                            <Subtitle> Phone </Subtitle>
                            <Caption> +254-729-055-061 </Caption>
                        </View>
                    </TouchableOpacity>
                </View>

            </ScrollView>
        )
    }

}













