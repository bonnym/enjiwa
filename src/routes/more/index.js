import React, { Component } from 'react'
import {
    TouchableOpacity,
    Image,
    ImageBackground,
    View,
    Text,
    ScrollView
} from 'react-native'

import {
    Button
} from 'react-native-elements'
import {
    Icon,
    Row,
    Card,
    GridRow
} from '@shoutem/ui';
import HeaderComponent from "../../common/header/index";

const logo = require('../../assets/logo.png');

export default class More extends Component{

    constructor(props){
        super(props);
        this.state = {

        }
    }

    render(){
        return(
            <View style={{flex: 1, backgroundColor: '#fff'}}>

                <ScrollView style={{marginTop: 20}}>
                    <Button
                        title="FAQ"
                        backgroundColor="dodgerblue"
                        onPress={() => {this.props.navigation.navigate('philately')}}
                    />

                    <Button
                        title="Private Policy"
                        backgroundColor="dodgerblue"
                        onPress={() => {this.props.navigation.navigate('notifications')}}
                    />

                    <Button
                        title="App Info"
                        backgroundColor="dodgerblue"
                        onPress={() => {this.props.navigation.navigate('scan')}}
                    />


                </ScrollView>
            </View>
        )
    }

}
