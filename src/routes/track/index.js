import React, { Component } from "react";
import { Text, StyleSheet} from "react-native";
import MapView from 'react-native-maps';
import { Container, InputGroup, Input, View }  from "native-base";

import { SearchBar, Icon } from 'react-native-elements'

import styles from './styles'
import HeaderComponent from "../../common/header/index";

export default class Tracker extends Component{

    render(){

        return(
            <View style={styles.container}>

                <MapView
                    provider={MapView.PROVIDER_GOOGLE}
                    style={styles.map}
                />
                <View style={styles.searchBox}>
                    <View style={styles.inputWrapper}>
                        <Text style={styles.label}>Track Parcel</Text>
                        <InputGroup>
                            <Icon name="search" size={15} color="#FF5E3A"/>
                            <Input
                                style={styles.inputSearch}
                                placeholder="Please enter your tracking pin"
                            />
                        </InputGroup>
                    </View>
                </View>
            </View>
        )
    }
}
