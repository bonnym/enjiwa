import { connect } from 'react-redux'
import Login from '../components/index'

import {
    login
} from '../modules/login_modules'

const mapStateToProps = (state) => {
    return {
        username: state.login.username,
        password: state.login.password
    };
};

const mapActionCreaters = {
    onLogin
};

// const mapDispatchToProps = (dispatch) => {
//     return {
//         onLogin: (email, password) => { dispatch(login(email, password)); },
//         onSignUp: (email, password) => { dispatch(signup(email, password)); }
//     }
// };

export default connect(mapStateToProps, mapActionCreaters)(Login);