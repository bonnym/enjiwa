import React, { Component, PropTypes } from 'react';
import { StyleSheet, Dimensions } from 'react-native';

const {height, width} = Dimensions.get('window');

const styles = StyleSheet.create({

    full: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#E0FFFE',
        width: width,
        height: height
    },

    text: {
        fontSize: 50,
        color: 'red'
    },

    button: {
        backgroundColor: '#0b7eff',
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 5,
        width: width
    },

    buttonText: {
        color: 'white',
        fontWeight: 'bold'
    },

    signUpText: {
        marginTop: 10,
        marginBottom: 20,
        color: '#3498DB',
        backgroundColor: 'transparent'
    },

    logoContainer: {
        alignItems: 'center',
        flexGrow: 1,
        justifyContent: 'center',
    },

    logo: {
        width: 200,
        height: 200,
        resizeMode: 'contain'
    },

    tagline: {
        fontSize: 20,
        color: 'dodgerblue',
        marginTop: 15,
        backgroundColor: 'transparent',
        fontStyle: 'italic',
    },

    input: {
        height: 55,
        backgroundColor: '#ABB7B7',
        color: '#FFF',
        paddingHorizontal: 20,
        marginBottom: 2,
        width: width
    },

    error: {
        color: 'red'
    },

    loader: {
        marginTop: 6
    }

});

module.exports = styles;


