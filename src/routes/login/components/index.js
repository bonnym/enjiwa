import React, { Component } from 'react';
import {
    Text,
    TextInput,
    View,
    Image,
    TouchableOpacity,
    AsyncStorage,
    KeyboardAvoidingView
} from 'react-native';

let Spinner = require('react-native-spinkit');

import styles from './styles';


class Login extends Component {

    constructor (props) {
        super(props);
        this.state = {
            route: 'Login',
            email: '',
            password: '',
            color: "#EB9532",
            user: []
        };
    }

    componentWillMount(){
        this._loadInitialState().done();

    }

    _loadInitialState = async () => {
        let value = await AsyncStorage.getItem('user');
        if ( value !== null){
            this.props.navigation.navigate('tracker');
        }
    };

    userLogin (e) {
        this.props.onLogin(this.state.email, this.state.password);
        e.preventDefault();

        let profile = {
            email: this.state.email,
            password: this.state.password
        };

        AsyncStorage.setItem('user', JSON.stringify(profile));
        let data = AsyncStorage.getItem('user');
        console.log('storage: ', data);

        if(this.state.email && this.state.password !== null) {
            this.props.navigation.navigate('Stamp')
        }

    }

    toggleRoute (e) {
        let alt = (this.state.route === 'Login') ? 'SignUp' : 'Login';
        this.setState({ route: alt });
        e.preventDefault();
    }

    render () {
        let alt = (this.state.route === 'Login') ? 'SignUp' : 'Login';
        return (
            <KeyboardAvoidingView behavior={'padding'} style={styles.full}>

                <View style={styles.logoContainer}>
                    <Image
                        style={styles.logo}
                        source={require('../../../assets/logo.png')}
                    />

                    <Text style={styles.tagline}>Posta popote</Text>
                </View>

                <TextInput
                    style={styles.input}
                    placeholder={'email'}
                    onChangeText={(text) => this.setState({ email: text })}
                    returnKeyType={'next'}
                    autoCapitalize={'none'}
                    autoCorrect={false}
                    keyboardType={'email-address'}
                    placeholderTextColor={'rgba(255,255,255,0.7)'}
                    value={this.state.email}
                />

                <View
                    style={styles.separator}
                >
                </View>


                <TextInput
                    style={styles.input}
                    placeholder={'Password'}
                    secureTextEntry
                    returnKeyType={'go'}
                    value={this.state.password}
                    autoCapitalize={'none'}
                    autoCorrect={false}
                    ref={(input) => this.passwordInput = input}
                    placeholderTextColor={'rgba(255,255,255,0.7)'}
                    onChangeText={(text) => this.setState({ password: text })}
                />

                <TouchableOpacity style={styles.button} onPress={(e) => this.userLogin(e)}>
                    {
                        this.props.loading &&
                        <Spinner style={styles.spinner} isVisible={this.props.loading} size={50} type={'Bounce'} color={this.state.color}/> ||
                        <Text style={styles.buttonText}>SIGN IN</Text>
                    }
                </TouchableOpacity>

                <Text style={styles.error}>
                    {this.props.error}
                </Text>

                <TouchableOpacity
                >
                    <Text style={styles.signUpText} onPress={() =>this.props.navigation.navigate('start')}>
                        Do not have an account? Sign Up
                    </Text>
                </TouchableOpacity>

            </KeyboardAvoidingView>
        );
    }
}

export default Login