import update from 'react-addons-update';
import constants from './constants';

// ----------------------------
// CONSTANTS
// ----------------------------

const {
    LOGIN,
} = constants;

// ======================== END ==============================

// ----------------------------
// ACTIONS
// ----------------------------

export function login(username, password) {

    return dispatch => {

        dispatch();
    };
}


// ======================== END ==============================

// ----------------------------
// ACTION HANDLERS
// ----------------------------

function handleLogin(state, action) {
    return update(state, {
        username: {
            $user: payload.username
        },
        password: {
            $password: payload.password
        }
    })
}


const ACTION_HANDLERS = {
    LOGIN: handleLogin
};

// ======================== END ==============================

const initialState = {
    user: {}
};

// ======================== END ==============================

export function LoginReducer(state = initialState, action) {
    const handler = ACTION_HANDLERS[action.type];

    return handler ? handler(state, action) : state;
}