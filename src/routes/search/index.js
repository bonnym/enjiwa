import React, { Component } from 'react'
import {
    ImageBackground,
    View,
    ScrollView
} from 'react-native'

import {
    Fab
} from 'native-base'

import {
    Screen,
    DropDownMenu,
    Text,
    NavigationBar,
    Image,
    Title,
    Icon,
    Row,
    Subtitle,
    Caption,
    Divider,
    TouchableOpacity
} from '@shoutem/ui'

import {
    Button,
    SearchBar,
    Avatar
} from 'react-native-elements'

const logo = require('../../assets/logo.png');


export default class Search extends Component{

    constructor(props){
        super(props);
        this.state = {
            cars: [
                { title: 'RECEIVED', value: 'RECEIVED' },
                { title: 'SENT', value: 'SENT' },
                { title: 'UNREAD', value: 'UNREAD' },
            ],
            active: false
        }
    }


    render(){
        return(
            <Screen style={{marginTop: 20}}>
                <SearchBar
                />
                <ScrollView>

                    <TouchableOpacity
                        style={{alignItems: 'center', flexDirection: 'row', backgroundColor: 'white'}}
                        onPress={() =>this.props.navigation.navigate('Stamp')}
                    >
                        <Avatar
                            rounded
                        />
                        <Image
                            style={{marginRight: 9}}
                            styleName="small rounded-corners"
                            source={{ uri: 'https://shoutem.github.io/img/ui-toolkit/examples/image-2.png' }}
                        />
                        <View styleName="vertical stretch space-between">
                            <Subtitle> Malaika Firth </Subtitle>
                            <Caption> 59000-00100 | Mombasa </Caption>
                        </View>
                    </TouchableOpacity>

                    <Divider styleName={'line'} />

                    <TouchableOpacity
                        style={{alignItems: 'center', flexDirection: 'row', backgroundColor: 'white'}}
                        onPress={() =>this.props.navigation.navigate('Stamp')}
                    >
                        <Avatar
                            rounded
                        />
                        <Image
                            style={{marginRight: 9}}
                            styleName="small rounded-corners"
                            source={{ uri: 'https://shoutem.github.io/img/ui-toolkit/examples/image-2.png' }}
                        />
                        <View styleName="vertical stretch space-between">
                            <Subtitle> Racheal Zane </Subtitle>
                            <Caption> 58080-00200 | Nakuru </Caption>
                        </View>
                    </TouchableOpacity>

                </ScrollView>
                <Fab
                    active={this.state.active}
                    direction="up"
                    containerStyle={{ }}
                    style={{ backgroundColor: '#5067FF' }}
                    position="bottomRight"
                    onPress={() => this.setState({ active: !this.state.active })}>
                    <Icon name="telegram" />
                    <Button style={{ backgroundColor: 'red' }}>
                        <Icon name="plus"/>
                    </Button>
                    <Button style={{ backgroundColor: 'dodgerblue' }}>
                        <Icon name="share" />
                    </Button>
                    <Button style={{ backgroundColor: 'orange' }}>
                        <Icon name="plus" />
                    </Button>
                </Fab>
            </Screen>
        )
    }

}













