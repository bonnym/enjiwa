import React, { Component } from 'react'
import {
    TouchableOpacity,
    Image,
    ImageBackground,
    View,
    Text,
    ScrollView
} from 'react-native'

import {
    Button
} from 'react-native-elements'
import {
    Icon,
    Row,
    Card,
    GridRow
} from '@shoutem/ui';
import HeaderComponent from "../../common/header/index";

const logo = require('../../assets/logo.png');

export default class Home extends Component{

    constructor(props){
        super(props);
        this.state = {

        }
    }

    render(){
        return(
            <View style={{flex: 1, backgroundColor: '#fff'}}>
                <HeaderComponent logo={logo}/>
                <ScrollView style={{marginTop: 20}}>
                    <Button
                        title="philately"
                        backgroundColor="dodgerblue"
                        onPress={() => {this.props.navigation.navigate('philately')}}
                    />
                    <Button
                        title="maps"
                        backgroundColor="dodgerblue"
                        onPress={() => {this.props.navigation.navigate('map')}}
                    />
                    <Button
                        title="Track"
                        backgroundColor="dodgerblue"
                        onPress={() => {this.props.navigation.navigate('tracker')}}
                    />

                    <Button
                        title="Notifications"
                        backgroundColor="dodgerblue"
                        onPress={() => {this.props.navigation.navigate('notifications')}}
                    />

                    <Button
                        title="Scan"
                        backgroundColor="dodgerblue"
                        onPress={() => {this.props.navigation.navigate('scan')}}
                    />

                    <Button
                        title="More"
                        backgroundColor="dodgerblue"
                        onPress={() => {this.props.navigation.navigate('more')}}
                    />

                    <Button
                        title="CHAT"
                        backgroundColor="dodgerblue"
                        onPress={() => {this.props.navigation.navigate('map')}}
                    />


                </ScrollView>
            </View>
        )
    }

}
