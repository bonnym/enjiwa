import React, { Component, PropTypes } from 'react'
import { connect } from 'react-redux'
import {
    Text
} from 'react-native'

import { Provider } from 'react-redux'
import Navigator from "../utils/routes";
import { addNavigationHelpers } from 'react-navigation'

const App = ({dispatch, nav }) => (
    <Navigator
        navigation={addNavigationHelpers({
            dispatch,
            state: nav,
        })}
    />
);

const mapStateToProps = state => ({
    nav: state.nav
});

const AppWithNavigation = connect(mapStateToProps)(App);


export default class AppContainer extends Component{

    // static propTypes = {
    //     store: PropTypes.object.isRequired
    // };

    render(){
        return(
            <Provider store={this.props.store}>
                <Navigator />
            </Provider>
        )
    }
}

